require 'rails_helper'

RSpec.describe 'ActiveRecord API V1' do
  shared_examples 'contains message key' do
    before { get base_url }

    it('contains string message key') { expect_json_types(message: :string) }
    it('return status correct code') { expect_status(expected_status_code) }
  end

  describe 'GET /invalid' do
    let(:base_url) { '/v1/invalid' }
    let(:expected_status_code) { 422 }

    it_behaves_like 'contains message key'
  end

  describe 'GET /notfound' do
    let(:base_url) { '/v1/notfound' }
    let(:expected_status_code) { 404 }

    it_behaves_like 'contains message key'
  end
end
