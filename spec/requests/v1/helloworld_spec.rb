require 'rails_helper'

RSpec.describe 'Helloworld API V1' do
  let(:base_url) { '/v1/hello' }

  describe 'GET /hello' do
    before do
      get base_url
    end

    it 'returns status code 200' do
      expect_status(200)
    end
  end
end
