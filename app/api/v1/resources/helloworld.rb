module V1
  module Resources
    class Helloworld < Base
      get :hello do
        { hello: 'world!' }
      end
    end
  end
end
