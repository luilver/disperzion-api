module V1
  module Resources
    class Artests < Base
      get :notfound do
        raise ActiveRecord::RecordNotFound
      end
      get :invalid do
        raise ActiveRecord::RecordInvalid
      end
    end
  end
end
