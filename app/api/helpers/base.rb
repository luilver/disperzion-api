# frozen_string_literal: true

module Helpers
  module Base
    def permitted_params
      @permitted_params ||= declared(params, include_missing: false)
    end

    def logger
      Rails.logger
    end
  end
end
